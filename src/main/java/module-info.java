module com.example.ei {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires java.sql;

    opens com.example.ei to javafx.fxml;
    exports com.example.ei;
}