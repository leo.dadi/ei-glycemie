package com.example.ei;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class OldApp extends Application {
    final int WINDOW_SIZE = 10;
    private ScheduledExecutorService scheduledExecutorService;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("JavaFX Realtime Chart Demo");

        //defining the axes
        final CategoryAxis xAxis = new CategoryAxis(); // we are gonna plot against time
        final NumberAxis yAxis= new NumberAxis();
        xAxis.setLabel("Time/s");
        xAxis.setAnimated(false); // axis animations are removed
        yAxis.setLabel("Value");
        yAxis.setAnimated(false); // axis animations are removed

        //creating the line chart with two axis created above
        final LineChart<String, Number> lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setTitle("Realtime JavaFX Charts");
        lineChart.setAnimated(false); // disable animations

        //defining a series to display data
        XYChart.Series<String, Number> series = new XYChart.Series<>();
        series.setName("Data Series");

        // add series to chart
        lineChart.getData().add(series);

        // setup scene
        Scene scene = new Scene(lineChart, 800, 600);
        primaryStage.setScene(scene);

        // show the stage
        primaryStage.show();

        // this is used to display time in HH:mm:ss format
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:.SSS");

        //Compteur des données
        final int[] i = {1};

        // setup a scheduled executor to periodically put data into the chart
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        // put dummy data onto graph per second
        scheduledExecutorService.scheduleWithFixedDelay(() -> {
            // get a random integer between 0-10
            Integer random = ThreadLocalRandom.current().nextInt(-5,5);
            Date now = new Date();
            // Update the chart
            Platform.runLater(() -> {
                //Get data from DB
                double data;
                try {
                    data = getData(i[0]);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                try {
                    fetchData(3);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                i[0] +=1;
                // put random number with current time
                series.getData().add(new XYChart.Data<>( simpleDateFormat.format(now), data));

                if (series.getData().size() > WINDOW_SIZE)
                    series.getData().remove(0);
            });
        }, 0, 3, TimeUnit.SECONDS);
    }

    public double getData(int i) throws SQLException {
        ResultSet rs;
        double result = 0;
        try {//Define connector to database
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/EI", "root", "mypassword");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/EI", "root", "mypassword");
            System.out.println("OK");
            String sql = "SELECT * FROM DONNEE LIMIT 1 OFFSET ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, i);
            System.out.println(stmt);
            rs = stmt.executeQuery();
            rs.next();
            result = rs.getDouble(1);
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }
    @Override
    public void stop() throws Exception {
        super.stop();
        scheduledExecutorService.shutdownNow();
    }

    public static String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }

    public static void fetchData(float time) throws Exception{
        String sql="SELECT%20*%20FROM%20Donnees%20WHERE%20temps>" + Float.toString(time);
        System.out.println(getHTML("http://eighth-duality-367908.ew.r.appspot.com/get?request="+sql));
    }
}
