package com.example.ei;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class App extends Application {
    final int WINDOW_SIZE = 10;
    private ScheduledExecutorService scheduledExecutorService;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("JavaFX Realtime Chart Demo");

        //defining the axes
        final NumberAxis xAxisGly = new NumberAxis(); // we are gonna plot against time
        final NumberAxis yAxisGly = new NumberAxis();
        xAxisGly.setLabel("Time/s");
        xAxisGly.setAnimated(false); // axis animations are removed
        yAxisGly.setLabel("Value");
        yAxisGly.setAnimated(false); // axis animations are removed

        //creating the line chart with two axis created above
        final LineChart<Number, Number> lineChartGly = new LineChart<>(xAxisGly, yAxisGly);
        lineChartGly.setTitle("Realtime JavaFX Charts");
        lineChartGly.setAnimated(false); // disable animations
        //defining a series to display data
        XYChart.Series<Number, Number> seriesGly = new XYChart.Series<>();
        seriesGly.setName("Data Series");
        // add series to chart
        lineChartGly.getData().add(seriesGly);

        // setup scene
        Scene scene = new Scene(lineChartGly, 800, 600);
        primaryStage.setScene(scene);

        // show the stage
        primaryStage.show();

        // this is used to display time in HH:mm:ss format
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:.SSS");

        //Compteur des données
        final int[] time = {-1};

        // setup a scheduled executor to periodically put data into the chart
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        // put dummy data onto graph per second
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            // get a random integer between 0-10
            Integer random = ThreadLocalRandom.current().nextInt(-5, 5);
            Date now = new Date();
            // Update the chart
            Platform.runLater(() -> {
                //Get data from DB
                String data;
                int newtime;
                int glycemie;
                try {
                    data = fetchData(time[0]);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                if (!data.equals("[]")) {
                    newtime = getTime(data);
                    glycemie = getGlycemie(data);
                    seriesGly.getData().add(new XYChart.Data<>(newtime, glycemie));
                    time[0] = newtime;
                }

                if (seriesGly.getData().size() > WINDOW_SIZE)
                    seriesGly.getData().remove(0);
            });
        }, 0, 4, TimeUnit.SECONDS);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        scheduledExecutorService.shutdownNow();
    }

    public static String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }

    public static String fetchData(int time) throws Exception {
        String sql = "SELECT%20*%20FROM%20Donnees%20WHERE%20temps>" + Integer.toString(time) + "%20LIMIT%201";
        return getHTML("http://eighth-duality-367908.ew.r.appspot.com/get?request=" + sql);
    }

    public int getTime(String string) {
        String[] donnee = string.split("\"");
        return Integer.valueOf(donnee[1]);
    }

    public int getGlycemie(String string) {
        String[] donnee = string.split("\"");
        return Integer.valueOf(donnee[3]);
    }

    public int getInsuline(String string) {
        String[] donnee = string.split("\"");
        return Integer.valueOf(donnee[5]);
    }
}
