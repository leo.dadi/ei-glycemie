package com.example.ei;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Point {

    public static void main(String[] args) throws ParseException {
        String startDate="12:06:34:33";
        String endDate="12:06:34:33";

        SimpleDateFormat sdf = new SimpleDateFormat("h:m:s:ms");
        System.out.println(sdf.parse(startDate).before(sdf.parse(endDate)));
    }

}
